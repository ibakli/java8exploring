package com.imad.projects.java8.stream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class StreamSamples {

	public static void main(String... strings) {
		
		List<String> salutationsList = new ArrayList<>();
		salutationsList.addAll(Arrays.asList("hey", "hi", "hello", "good morning" ,"good evening"));
		
		Predicate<String> beginsWith = s -> s.startsWith("h");
		
		//apply filter (begin with 'h', apply upperCase to elements, sort them and print the result
		salutationsList.stream()
						.filter(beginsWith)  // apply predicate start with 'h'
						.map(String::toUpperCase) //apply upperCase
						.sorted() // sort the list
						.forEach(System.out::println); // print resulting elements
	}
}
