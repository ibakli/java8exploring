package com.imad.projects.java8.lembda;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class MethodeReference {

	public static void main(String... strings ) {
		
		// consumer : printer refers to method system.out.println(String); <=> (String x) -> System.out.println(x);  
		Consumer<String> printer = System.out::println;
		
		// predicate : listContainsElem refers to a method that checks if the list defined contains a string 
		List<String> listStrings = new ArrayList<>();
		listStrings.addAll(Arrays.asList("hey", "hi", "hello"));
		Predicate<String> listContainsElem = listStrings::contains;
		
		
		printer.accept("is the list contains 'hey' string ? response is : " + listContainsElem.test("hey"));
	}
}
